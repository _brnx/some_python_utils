import errno
import inotify.adapters
import os

def py_tail(absfname):

  absfname = os.path.realpath(absfname)
  dirname = os.path.dirname(absfname)
  fname=os.path.basename(absfname)
  if not os.path.isdir(dirname):
    raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), dirname)

  i = inotify.adapters.Inotify()
  i.add_watch(dirname)

  if os.path.isfile(absfname):
    f = open(absfname)
    f.seek(0, os.SEEK_END)
  
  for event in i.event_gen(yield_nones=False):
    (_, events_type, path, filename) = event
    if fname == filename:
      if 'IN_MODIFY' in events_type:
        line=f.readline()
        if line :
          yield line
      elif 'IN_CLOSE_WRITE' in events_type:
        #reset 
        f.seek(0, os.SEEK_END)
      elif 'IN_DELETE' in events_type:
        f.close()
      elif 'IN_CREATE' in events_type:
        f = open(absfname)
        f.seek(0, os.SEEK_END)

if __name__ == "__main__":
  import sys
  try:
    for line in py_tail(sys.argv[1]):
      print(f"{line.strip()}")
  except KeyboardInterrupt:
    pass
