import sys
import os
import time


def py_tail(fname):
  with open(fname) as f:
    f.seek(0, os.SEEK_END)
    while True:
      line = f.readline()
      if not line:
        time.sleep(0.1)
        continue
      yield line

if __name__ == "__main__":
  try:
    for line in py_tail(sys.argv[1]):
      print(f"{line.strip()}")
  except KeyboardInterrupt:
    pass
